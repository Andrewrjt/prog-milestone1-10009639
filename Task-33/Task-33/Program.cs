﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_33
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hi how many students do you have this semester?");
            var group = double.Parse(Console.ReadLine());
            double group1 = group/28;
            var check = group1 % 1;
            var final = (int)group1;//this converts to a int so it is cleaner//
            if (check==0)
            {
                Console.WriteLine($"you need {final}");
            }
            else
            {
                Console.WriteLine($"you need {final + 1}");
            }
            Console.ReadLine();
        }
    }
}
