﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_24
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = "";
            var repeat = true;
            do
            {
                Console.Clear();
                Console.WriteLine($"******************************");
                Console.WriteLine($"          MAIN MENU           ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"    1. Option 1               ");
                Console.WriteLine($"    2. Option 2               ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"    Press M or m for menus    ");
                Console.WriteLine($"                              ");
                Console.WriteLine($"******************************");
                Console.WriteLine();
                Console.WriteLine($"Please select an option");
                menu = (Console.ReadLine());
                if (menu == "1")
                {
                    Console.Clear();
                    Console.WriteLine("this is option 1 press M or m to go back to menu");
                    menu = Console.ReadLine();
                    if (menu == "m" || menu == "M")
                    {
                        repeat = true;
                    }
                    else
                    {
                        Console.WriteLine("It needed to be M or m");
                    }
                }
                if (menu == "2")
                {
                    Console.Clear();
                    Console.WriteLine("this is option 2press M or m to go back to menu");
                    menu = Console.ReadLine();
                    if (menu == "m" || menu == "M")
                    {
                        repeat = true;
                    }
                    else
                    {
                        Console.WriteLine("It needed to be M or m");
                    }
                }
            } while (repeat == true);

            
        }
    }
}
