﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello can I please have the month you were born on please?");
            var month = Console.ReadLine();
            Console.WriteLine("Thanks, Can I have the Day of the month you were born in?");
            var day = Console.ReadLine();
            Console.WriteLine($"Awesome so you were born on the {day}th of {month}");
            Console.ReadLine();
        }
    }
}
