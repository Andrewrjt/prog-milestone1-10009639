﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            var price = new List<double> { };
            var counter = 3;
            var i = 0;

            Console.WriteLine("Hi can I please get the three prices and I will add them up");

            for (i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"Can i please get the {a} price");
                double b = double.Parse(Console.ReadLine());
                price.Add(b);
            }
            double[] prices = price.ToArray();
            Console.Clear();
            Console.WriteLine($"${prices[0]}");
            Console.WriteLine($"${prices[1]}");
            Console.WriteLine($"${prices[2]}");
            var sum = (prices[0] + prices[1] + prices[2]) * 1.15;
            var round = Math.Round(sum, 2);
            Console.WriteLine($"${round}");
        }
    }
}
