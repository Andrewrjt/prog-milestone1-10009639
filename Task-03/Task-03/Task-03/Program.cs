﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            const double mil = 0.621371;
            const double km = 1.609344;
            Console.WriteLine("Hi do you want to convert kilometers or miles?");
            var milorkm = Console.ReadLine();
            switch (milorkm)
            {
                case "kilometers":
                    var a = 0;
                    Console.WriteLine("How many kilometers?");
                    a = int.Parse(Console.ReadLine());
                    Console.WriteLine($"Ok that is {a * mil}");
                    break;
                case "miles":
                    var b = 0;
                    Console.WriteLine("How many miles:");
                    b = int.Parse(Console.ReadLine());
                    Console.WriteLine($"ok that is {b * km} kilometers");
                    break;
                default:
                    Console.WriteLine("it needs to be miles or kilometers");
                    break;
            }
            Console.ReadLine();
        }
    }
}
