﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What year would you like me to check is a leap year?");
            var year = int.Parse(Console.ReadLine());
            var leap = year;
            leap %= 4;
            if (leap == 0)
            {
                Console.WriteLine("It is a leap year");
            }
            else
            {
                Console.WriteLine("this isnt a leap year");
            }
        }
    }
}
