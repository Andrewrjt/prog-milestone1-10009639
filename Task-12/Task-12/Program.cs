﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {
          
            Console.WriteLine("Hi what number do you want to check to see if it is a even number or a odd number?");
            var text = Console.ReadLine();
            int counter;
            bool resolve = int.TryParse(text, out counter);
            if (resolve == true)
            {
                if ((counter & 1) == 0)
                {
                    Console.WriteLine("it is a even number");
                }
                else
                {
                    Console.WriteLine("it is a odd number");
                }
            }
            else
            {
                Console.WriteLine("It needs to be a number");
            }
            Console.ReadLine();
        }
    }
}
